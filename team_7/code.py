import string
import random

 
def random_password():
    length = int(input('''Welcome to random password generator.
    Enter your password length '''))
    print('''Choose character set for password from these :
         1. Letters
         2. Digits
         3. Special characters
         4. Exit''')
    characterList = ""

    while(True):
        choice = int(input("Choose which characters you want in password: "))
        if(choice == 1):
            characterList += string.ascii_letters
        elif(choice == 2):
            characterList += string.digits
        elif(choice == 3):
            characterList += string.punctuation
        elif(choice == 4):
            break
        else:
            print("Please pick a valid option!")
    

    password = []
    for i in range(length):
        randomchar = random.choice(characterList)
        password.append(randomchar)
    print("The random password is " + "".join(password))

random_password()