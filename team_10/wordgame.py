import random



def guess_game():
    print("**started**")

word = ['programming', 'tiger', 'lamp', 'television',
           'laptop', 'water', 'microscope', 'doctor', 
           'youtube','projects','python','india','source',
           'file','gitlab']
random_word = random.choice(word)
#print('our random word', random_word)

name = input("what is your name")
print("good luck !",name)

user_guesses = ''
turns = 10


while turns > 0:
    wrong_guesses = 0
    for character in random_word:
        if character in user_guesses:
            print(f"Correct guess: {character}")
        else:
            wrong_guesses += 1
            print('_')

   
    if wrong_guesses == 0:
        print("Correct.")
        correct = 10 
        print("you win the game u scored 10 points")
        print(f"Word : {random_word}")
        break
    guess = input('Make a guess: ')
    user_guesses += guess


    if guess not in random_word:
        turns -= 1
        print(f"Wrong. You have {turns} more chances")

        if turns == 0:
            print('game over')


guess_game()