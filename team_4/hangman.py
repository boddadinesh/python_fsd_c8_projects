#importing 
import random,hangman_stages,sys

obj = hangman_stages.Stages()

print("\nWelcome to Hangman! Let's see if you can guess this word!\n")
print("Hint :: Fruits and Animals \n")

#stored data of game  \\"tiger", "tree", "underground", "giraffe", "chair"\\
words = ["fish","tiger","giraffe","pineapple","apple","orange","banana","lion","elephant","carrot"]
remaining_attempts = 6    #zero stages of hangman 
guessed_letters = ""      #input guessed letters added to empty string

#randomly choices one word from list
def select_word(words):
    return random.choice(words)
# print(select_word(words))

secret_word = select_word(words)   # word randomly selected from list
#print(secret_word)

def print_secret_word(secret_word, guessed_letters): #prints secret_word in "_"  or letters
    for letter in secret_word:
        if letter in guessed_letters:
            print("{} ".format(letter),end="")
        else:
            print(" _ ",end = '')
    print("\n")
# print_secret_word(secret_word, guessed_letters)

#input guess letter in secret_word / only single letter 
def is_guess_in_secret_word(guess, secret_word):
    if len(guess) > 1 or not guess.isalpha():
        print("Only single letters are allowed. Unable to continue...")
        sys.exit()
    else:
        if guess in secret_word:
            return True
        else:
            return False
# print(is_guess_in_secret_word(guess, secret_word))

def get_unique_letters(word):
    return "".join(set(word))
# print(get_unique_letters(secret_word))


while remaining_attempts > 0 and len(guessed_letters) < len(get_unique_letters(secret_word)):
    guess = input("Guess a letter: ")     #pass letter
    guess_in_secret_word = is_guess_in_secret_word(guess, secret_word)
    #checks same letter or different letter is guessed and correct or not
    if guess_in_secret_word:
        if guess in guessed_letters:
            print("You have already guessed the letter {}".format(guess))
        else:
            print("Yes! The letter {} is part of the secret word".format(guess))
            guessed_letters += guess
    else:
        print("No! The letter {} is not part of the secret word".format(guess))
        remaining_attempts -= 1

    print(obj.get_hangman_stage(remaining_attempts))
    print("\n{} attempts remaining\n".format(remaining_attempts))
    print_secret_word(secret_word, guessed_letters)
    print("\n\nNumber of letters guessed: {}\n".format(len(guessed_letters)))


if len(guessed_letters) == len(get_unique_letters(secret_word)):
    print("***WON GAME***\n")
else:
    print("***GAME LOST***\n")