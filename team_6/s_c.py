## text analyzer
class Analyzer:
    from textblob import TextBlob as Tb

    def __init__(self,text):
        self.text = text
    
    # Creates a file and stores the given text in it. 
    def store_file(self):
        self.file_ = open('file1.txt','w')
        self.file_.write(self.text)
        self.file.close()

    # Counts number of words in the given sentence.
    def count_word(self): 
        dict1 = {}
        self.list_of_words = []
        for each in self.text.split(' '):
            if each.isalnum():
                self.list_of_words.append(each)
        for each in self.list_of_words:
            if self.list_of_words.count(each)>1:
                dict1[each] = self.list_of_words.count(each)
        self.word_count = len(self.list_of_words)
        return f'The number of frequently used words are {len(dict1.keys())}\nand number of words used in the sentence are {self.word_count}'

    # Counts numbers of sentences
    def count_sentences(self):
        self.list_of_sentences = self.text.split('.')
        return f'The number of sentences used in the text are {len(self.list_of_sentences)}'

    # Counts numbers of syllables
    def count_syllables(self):
        import syllables as sy
        list_syllable_count = []
        for each in set(self.list_of_words):
            list_syllable_count.append(sy.estimate(each))
        return f'The total number of syllables in the text are {len(list_syllable_count)}'

    # Gives us the lexical density of the langauage used.
    # DEF : Lexical density estimates the linguistic complexity in a written or spoken 
    #       composition from the functional words (grammatical units) and content words.
    def lexical_density(self):
        list_pos = list(Analyzer.Tb(self.text).tags)
        noun_list = []
        verb_list = []
        adverb_list = []
        adjective_list = []
        for each in list_pos:
            if each[1] in ['NN','NNS','NNP','NNPS']:
                noun_list.append(each[0])
            elif each[1] in ['VB','VBD','VBG','VBN','VBP','VBZ']:
                verb_list.append(each)
            elif each[1] in ['RB','RBR','RBS']:
                adverb_list.append(each)
            elif each[1] in ['JJ','JJR','JJS']:
                adjective_list.append(each)
        lex_den = format(((len(noun_list)+len(verb_list)+len(adverb_list)+len(adjective_list))/(self.word_count)) * 100,'.2f')
        return f'The lexical density of the text is {lex_den} %.'

    #This method results are used to decide whether the context the given text is objective or subjective.
    def sentiment_analysis(self):
        subjective_count = 0
        objective_count = 0
        for each in self.list_of_sentences:
            y = Analyzer.Tb(each)
            if y.sentiment.polarity > y.sentiment.subjectivity:
                objective_count += 1
            else:
                subjective_count += 1
        return f'There are {objective_count} number of objective sentences in the text.\nThere are {subjective_count} number of subjective sentences in the text.'

                 




analaysis1 = Analyzer('''Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. Its high-level built in data structures, combined with dynamic typing and dynamic binding, make it very attractive for Rapid Application Development, as well as for use as a scripting or glue language to connect existing components together. Python's simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse. The Python interpreter and the extensive standard library are available in source or binary form without charge for all major platforms, and can be freely distributed.Often, programmers fall in love with Python because of the increased productivity it provides. Since there is no compilation step, the edit-test-debug cycle is incredibly fast. Debugging Python programs is easy: a bug or bad input will never cause a segmentation fault. Instead, when the interpreter discovers an error, it raises an exception. When the program doesn't catch the exception, the interpreter prints a stack trace. A source level debugger allows inspection of local and global variables, evaluation of arbitrary expressions, setting breakpoints, stepping through the code a line at a time, and so on. The debugger is written in Python itself, testifying to Python's introspective power. On the other hand, often the quickest way to debug a program is to add a few print statements to the source: the fast edit-test-debug cycle makes this simple approach very effective.
''')
print(analaysis1.count_word())
print(analaysis1.count_sentences())
print(analaysis1.count_syllables())
print(analaysis1.lexical_density())
print(analaysis1.sentiment_analysis())
        



