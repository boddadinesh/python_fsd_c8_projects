import datetime

class Card:
    C_Month =int(datetime.datetime.now().strftime("%m"))
    C_Year = int(datetime.datetime.now().strftime("%y"))
    def __init__(self):
        self.card_num = input('Enter the card number: ')
        self.name_on_card = input("Enter The Card Holder's Name: ")
        self.bank_name = input('Enter the Bank Name: ')
        self.month = int(input('Enter the Expiry Month: '))
        self.year = int(input('Enter the Expiry Year: '))
        self.is_valid_card()
        self.company()
    
    def is_valid_card(self):
        sum_odd_digits=0
        sum_even_digits=0
        total=0
        self.result = ''
        self.temp_card_number = self.card_num
        self.temp_card_number = self.temp_card_number.replace("-", "")
        self.temp_card_number = self.temp_card_number.replace(" ", "")
        self.temp_card_number = self.temp_card_number[::-1]

        for x in self.temp_card_number[::2]:
            sum_odd_digits += int(x)

        for x in self.temp_card_number[1::2]:
            x=int(x)*2
            if x>=10:
                sum_even_digits += (1+(x%10))
            else:
                sum_even_digits += x

        total = sum_odd_digits + sum_even_digits

        if total %10 == 0 and 0 < self.year > Card.C_Year:
            self.result = "Valid Card"

        elif self.year == Card.C_Year and 0 < self.month <= Card.C_Month:
            self.result = "Valid Card"

        else:
            self.result = "Invalid Card"
    
    def company(self):
        self.comp = 'Company: '
        
        if self.result == "Valid Card":
            if str(self.card_num).startswith('4'):
                self.comp +="Visa Card"
            elif str(self.card_num).startswith(('50','67','58','63')):
                self.comp +="Maestro Card"
            elif str(self.card_num).startswith('5'):
                self.comp +="Master Card"
            elif str(self.card_num).startswith('37'):
                self.comp +="American Express Card"
            elif str(self.card_num).startswith('62'):
                self.comp +="UnionPay Card"
            elif str(self.card_num).startswith('7'):
                self.comp +="Gasoline Card"
            

    def __repr__(self):
        if self.result == "Valid Card":
            return str(self.card_num) + ' is a '+ self.result + ' and the belongs to the ' + self.comp
        else:
            return str(self.card_num) + ' is a '+ self.result


Card()

'''
4535907183276072 -> Valid card
4535907183276071 -> Invalid Card 

'''
